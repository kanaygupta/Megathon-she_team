class CreateAssigns < ActiveRecord::Migration[5.1]
  def change
    create_table :assigns do |t|
      t.references :complaint, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
