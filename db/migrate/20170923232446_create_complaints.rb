class CreateComplaints < ActiveRecord::Migration[5.1]
  def change
    create_table :complaints do |t|
      t.string :medium
      t.string :identification
      t.string :complainant_name
      t.string :complainant_phone
      t.string :complainant_gender
      t.integer :urgency
      t.string :status
      t.string :polic_station
      t.string :complaint_type
      t.string :language

      t.timestamps
    end
  end
end
