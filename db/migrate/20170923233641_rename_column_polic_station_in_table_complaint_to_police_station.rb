class RenameColumnPolicStationInTableComplaintToPoliceStation < ActiveRecord::Migration[5.1]
  def change
    rename_column :complaints, :polic_station, :police_station
  end
end
