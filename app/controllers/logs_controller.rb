class LogsController < ApplicationController
  def create
    user_id = current_user.id
    complaint_id = params[:complaint_id]
    content = logs_params[:content]
    if Log.create(user_id: user_id, complaint_id: complaint_id, content: content)
      flash[:success] = "Success. Added Log"
      redirect_to home_path
    else
      flash[:danger] = "Unsuccessfull. Try again"
      redirect_to home_path
    end
  end

  private
  def logs_params
    params.require(:log).permit(:complaint_id, :user_id, :content)
  end
end
