class ComplaintsController < ApplicationController
  require "google/cloud/translate"

  def index
    @complaints = Complaint.all
  end

  def new
    @complaint = Complaint.new
  end

  def show
    @complaint = Complaint.find(params[:id])
    @logs = Log.where(complaint_id: params[:id])
  end

  def create
    @complaint = Complaint.new(complaints_params)
    if @complaint.save
      flash[:success] = "Success. Added Complaint"
      redirect_to home_path
    else
      render 'new'
    end
  end

  private
  def complaints_params
    params.require(:complaint).permit(:medium, :identification, :complainant_name, :complainant_phone,
                                     :complainant_gender, :urgency, :status, :police_station, :complaint_type, :language)
  end
end
